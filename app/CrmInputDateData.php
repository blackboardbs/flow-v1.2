<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CrmInputDateData extends Model
{
    protected $table = 'crm_input_date_data';
}

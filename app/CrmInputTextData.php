<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CrmInputTextData extends Model
{
    protected $table = 'crm_input_text_data';
}

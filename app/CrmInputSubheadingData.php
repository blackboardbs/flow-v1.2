<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CrmInputSubheadingData extends Model
{
    protected $table = 'crm_input_subheading_data';
}

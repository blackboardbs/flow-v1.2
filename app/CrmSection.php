<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CrmSection extends Model
{
    use SoftDeletes;

    protected $table = 'crm_sections';
    protected $dates = ['deleted_at'];

    public function crm_section_input()
    {
        return $this->hasMany('App\CrmSectionInputs', 'crm_section_id')->orderBy('order');
    }

    public function crm_section_inputs()
    {
        return $this->hasMany('App\CrmSectionInputs', 'crm_section_id')->orderBy('order');
    }

    public function crm()
    {
        return $this->belongsTo(Crm::class)->withTrashed();
    }

    public function tabs()
    {
        return $this->belongsTo('App\CrmTab', 'tab_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CrmSectionInputs extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function input()
    {
        return $this->morphTo();
    }

    public function getCrmTypeName()
    {
        //activity type hook
        switch ($this->input_type) {
            case 'App\CrmInputText':
                return 'text';
                break;
            case 'App\CrmInputHeading':
                return 'heading';
                break;
            case 'App\CrmInputSubheading':
                return 'subheading';
                break;
            case 'App\CrmInputAmount':
                return 'amount';
                break;
            case 'App\CrmInputPercentage':
                return 'percentage';
                break;
            case 'App\CrmInputInteger':
                return 'integer';
                break;
            case 'App\CrmInputVideo':
                return 'video';
                break;
            case 'App\CrmInputTextarea':
                return 'textarea';
                break;
            case 'App\CrmInputDropdown':
                return 'dropdown';
                break;
            case 'App\CrmInputRadio':
                return 'radio';
                break;
            case 'App\CrmInputCheckbox':
                return 'checkbox';
                break;
            case 'App\CrmInputDate':
                return 'date';
                break;
            case 'App\CrmInputBoolean':
                return 'boolean';
                break;
            default:
                return 'error';
                break;
        }
    }

    public function getCrmTypeDisplayName()
    {
        //activity type hook
        switch ($this->input_type) {
            case 'App\CrmInputText':
                return 'Free text';
                break;
            case 'App\CrmInputAmount':
                return 'Amount';
                break;
            case 'App\CrmInputPercentage':
                return 'Percentage';
                break;
            case 'App\CrmInputInteger':
                return 'Integer';
                break;
            case 'App\CrmInputVideo':
                return 'Video';
                break;
            case 'App\CrmInputTextarea':
                return 'Textarea';
                break;
            case 'App\CrmInputBoolean':
            case 'App\CrmInputDropdown':
                return 'Dropdown';
                break;
            case 'App\CrmInputRadio':
                return 'Radio';
                break;
            case 'App\CrmInputCheckbox':
                return 'Checkbox';
                break;
            case 'App\CrmInputDate':
                return 'Date';
                break;
            default:
                return 'error';
                break;
        }
    }

    public function crm(){
        return $this->belongsTo(CrmSection::class,'crm_section_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CrmInputBooleanData extends Model
{
    protected $table = 'crm_input_boolean_data';
}

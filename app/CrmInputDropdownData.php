<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CrmInputDropdownData extends Model
{
    protected $table = 'crm_input_dropdown_data';

    public function item()
    {
        return $this->hasOne('App\CrmInputDropdownItem', 'id', 'crm_input_dropdown_item_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CrmInputDropdown extends Model
{
    public function crm_section_input()
    {
        return $this->morphOne('App\CrmSectionInputs', 'input');
    }

    public function items(){
        return $this->hasMany('App\CrmInputDropdownItem');
    }

    public function data()
    {
        return $this->hasMany('App\CrmInputDropdownData');
    }

    public function valuess()
    {
        return $this->hasMany('App\CrmInputDropdownData','crm_input_dropdown_id');
    }
}

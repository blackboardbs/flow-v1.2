<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CrmInputBoolean extends Model
{
    public function crm_section_input()
    {
        return $this->morphOne('App\CrmSectionInputs', 'input');
    }

    public function data()
    {
        return $this->hasMany('App\CrmInputBooleanData');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormArea extends Model
{
    public function office()
    {
        return $this->belongsTo('App\Office', 'office_id');
    }
}

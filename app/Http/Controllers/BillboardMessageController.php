<?php

namespace App\Http\Controllers;

use App\BillboardMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BillboardMessageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request){

        $message = new BillboardMessage();
        $message->message = $request->input('billboard_message');
        $message->user_id = Auth::id();
        $message->save();

        return $request->input('billboard_message');
    }

    public function show(Request $request, $message_id){
        $message = BillboardMessage::find($message_id);

        $message_arr = [
            "id" => $message->id,
            "message" => $message->message
        ];

        return response()->json($message_arr);
    }
}

<?php

namespace App\Http\Controllers;

use App\Activity;
use App\ActivityInClientBasket;
use App\Area;
use App\FormArea;
use App\FormGroup;
use App\FormInputDropdownItem;
use App\FormInputRadioItem;
use App\FormInputCheckboxItem;
use App\FormLog;
use App\Forms;
use App\FormSection;
use App\FormSectionInputInClientBasket;
use App\FormSectionInputs;
use App\Office;
use App\User;
use Illuminate\Http\Request;
use App\Client;
use Illuminate\Support\Facades\Auth;
use PhpOffice\PhpWord\TemplateProcessor;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Step;
use App\Log;
use App\FormInputTextData;
use App\FormInputTextareaData;
use App\FormInputDropdownData;
use App\FormInputDateData;
use App\FormInputBooleanData;
use App\FormInputCheckboxData;
use App\FormInputRadioData;
use App\ClientForm;
use PhpOffice\PhpWord\Writer\PDF\DomPDF;
use PhpOffice\PhpWord\Settings;
use Illuminate\Support\Facades\Response;

class FormsController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
        //return $this->middleware('auth')->except('index');
    }

    public function groupIndex(Request $request)
    {
        $groups = [];

        if ($request->has('q')) {
            $form_groups = FormGroup::where('name', 'LIKE', "%" . $request->input('q') . "%")->get();
        } else {
            $form_groups = FormGroup::get();
        }

        foreach($form_groups as $form_group){
            $groups[][$form_group->name] = [
                'id' => $form_group->id,
                'name' => $form_group->name,
                'fcount' => Forms::where('form_group_id',$form_group->id)->count(),
                'created_at' => $form_group->created_at,
                'updated_at' => $form_group->updated_at
            ];
        }

        ksort($groups);

        $groups[]['None'] = [
            'id' => '0',
            'name' => 'None',
            'fcount' => Forms::where('form_group_id',0)->count(),
            'created_at' => 'N/A',
            'updated_at' => 'N/A'
        ];

        //$groups += array_splice($groups,array_search('None',array_keys($groups)),1);

        $parameters = [
            'forms' => $groups,
            'type_name' => 'Forms'
        ];

        return view('forms.groupindex')->with($parameters);
    }

    public function groupCreate(Request $request)
    {
        return view('forms.groupcreate');
    }

    public function groupStore(Request $request){
        $process = new FormGroup;
        $process->name = $request->input('name');
        $process->save();

        return redirect(route('formsgroup.index'))->with('flash_success', 'Form category created successfully.');
    }

    public function groupEdit($group_id){
        $group = FormGroup::where('id',$group_id)->get();

        $parameters = [
            'form_groups' => $group
        ];

        return view('forms.groupedit')->with($parameters);
    }

    public function groupUpdate(Request $request,$group_id){
        $group = FormGroup::find($group_id);
        $group->name = $request->input('name');
        $group->save();

        return redirect(route('formsgroup.index'))->with('flash_success', 'Form category updated successfully.');
    }

    public function groupDestroy(Request $request,$group_id){

        $processes = Process::where('process_group_id',$group_id)->get();

        if($processes->count()>0){

            return redirect(route('processes.index',$group_id))->with('flash_danger', 'There are still active sub-processes assigned to this process.');
        }

        $process = ProcessGroup::find($group_id);

        $process->destroy($group_id);
        $process_type_id = $request->has('t') ? $request->input('t') : 1;

        return redirect(route('processesgroup.index', ['t' => $process_type_id]))->with(['flash_success' => 'Sub-process deleted successfully.']);
    }

    public function index(Request $request,$group_id){

        $areas = auth()->user()->areas()->get()->map(function ($area){
            return $area->id;
        })->toArray();

        $offices = auth()->user()->offices()->get()->map(function ($office){
            return $office->id;
        })->toArray();

        $forms = Forms::with(['sections','fgroup','form_area.office.area.region.division'])
            ->where('form_group_id',$group_id);

        if ($request->has('q')) {
            $forms->where('name', 'LIKE', "%" . $request->input('q') . "%");
        }

        $forms = $forms->get();

        $parameters = [
            'form_group' => ($group_id == 0 ? 0 : FormGroup::where('id',$group_id)->first()),
            'form_groups' => FormGroup::pluck('name','id'),
            'forms' => $forms,
            'type_name' => 'Forms',
            'type_name_single' => 'Form'
        ];

        return view('forms.index')->with($parameters);
    }

    public function create(Request $request,$group_id){

        $parameters = [
            'form_group' => $group_id,
            'form_groups' => FormGroup::orderBy('name')->pluck('name','id')->prepend('Please Select','0'),
            'areas' => Area::whereHas('region.division')->orderBy('name')->get()->pluck('name', 'id')->prepend('All','0'),
            'offices' => Office::orderBy('name')->get()->pluck('name', 'id'),
            'type_name' => 'Forms'
        ];

        return view('forms.create')->with($parameters);
    }

    public function store(Request $request,$group_id){
        $global = 0;

        foreach($request->input('area') as $area){
            if($area == '0'){

                $form = new Forms;
                $form->name = $request->input('name');
                $form->global_form = 1;
                $form->form_group_id = $request->input('category');
                $form->not_started_colour = str_replace('#', '', $request->input('not_started_colour'));
                $form->started_colour = str_replace('#', '', $request->input('started_colour'));
                $form->completed_colour = str_replace('#', '', $request->input('completed_colour'));
                $form->form_group_id = $group_id;
                $form->save();

                $offices = Office::get();

                foreach ($offices as $office){
                    $form_area = new FormArea();
                    $form_area->form_id = $form->id;
                    $form_area->area_id = $office->area->id;
                    $form_area->office_id = $office->id;
                    $form_area->save();
                }
            } else {
                $form = new Forms();
                $form->name = $request->input('name');
                $form->form_group_id = $request->input('category');
                $form->not_started_colour = str_replace('#', '', $request->input('not_started_colour'));
                $form->started_colour = str_replace('#', '', $request->input('started_colour'));
                $form->completed_colour = str_replace('#', '', $request->input('completed_colour'));
                $form->form_group_id = $group_id;
                $form->save();

                if($request->has('office') && count($request->input('office')) >=0) {
                    $offices = Office::whereHas('area', function ($q) use ($area) {
                        $q->where('id', $area);
                    })->whereIn('id', $request->input('office'))->get();

                    //dd($offices);
                    foreach ($offices as $office) {
                        $form_area = new FormArea();
                        $form_area->form_id = $form->id;
                        $form_area->area_id = $office->area->id;
                        $form_area->office_id = $office->id;
                        $form_area->save();
                    }
                } else {
                    $offices = Office::whereHas('area',function ($q) use ($area) {
                        $q->where('id', $area);
                    })->get();

                    $offices2 = Office::whereHas('area',function ($q) use ($area) {
                        $q->where('id', $area);
                    })->whereIn('id',$request->input('office'))->get();


                    foreach ($offices2 as $office){
                        $form_area = new FormArea();
                        $form_area->form_id = $form->id;
                        $form_area->area_id = $office->area->id;
                        $form_area->office_id = $office->id;
                        if(count($offices) == count($offices2)) {
                            $global = 1;
                        } else {
                            $global = 0;
                        }
                        $form_area->save();
                    }
                }
            }
        }

        return redirect(route('forms.show',[$group_id,$form]))->with('flash_success', 'Form create successfully.');
    }

    public function edit($group_id,$form_id)
    {
        $form_area = [];

        $fas = FormArea::select('area_id')->where('form_id',$form_id)->distinct()->get();

        if(count($fas) == count(Area::whereHas('region.division')->orderBy('name')->get())){
            $form_area[0] = '0';
        } else {
            foreach ($fas as $fa) {

                array_push($form_area, $fa->area_id);

            }
        }

        //dd($process_area);

        $paramaters = [
            'form' => Forms::where('id',$form_id)->first(),
            'form_groups' => FormGroup::orderBy('name')->pluck('name','id')->prepend('Please Select','0'),
            'areas' => Area::whereHas('region.division')->orderBy('name')->get()->pluck('name', 'id')->prepend('All','0'),
            'offices' => Office::orderBy('name')->get()->pluck('name', 'id'),
            'form_areas' => $form_area
        ];

        //dd($paramaters);

        return view('forms.edit')->with($paramaters);
    }

    public function update($formid,Request $request){
        $form = Forms::find($formid);
        if ($request->has('name')) {
            $form->name = $request->input('name');
        }

        $form->save();

        return redirect(route('forms.show',$form))->with('flash_success', 'Form updated successfully.');
    }

    public function show($formgroup, $form, Request $request)
    {

        $form = Forms::with(['sections'=> function ($q){
            $q->whereNull('deleted_at');
        }],'form_area.office.area.region.division')->where('id',$form)->first();

        return view('forms.show')->with(['formgroup'=>$formgroup,'forms' => $form]);
    }

    public function destroy($formid){

        Forms::destroy($formid);

        return redirect(route('forms.index'))->with('flash_success', 'Form deleted successfully.');
    }

    public function DynamicForm(Client $client,$form_id,$section_id){

        /*$client = Client::find($client_id);*/

        if($section_id == 0) {
            $client_forms = ClientForm::where('id',$form_id)->first();
            $form = Forms::withTrashed()->where('id',$client_forms->dynamic_form_id)->first();
            $form_step_first = FormSection::where('form_id',$client_forms->dynamic_form_id)->orderBy('order')->first()->id;
            $form_section = FormSection::find($form_step_first);
        } else {
            $form = Forms::withTrashed()->where('id',$form_id)->first();
            $form_section = FormSection::find($section_id);
        }

        $step = Step::withTrashed()->find(1);
        $process_progress = $client->getProcessStepProgress($step);
        $form_progress = $client->getFormsStepProgress($form_section);

        $client_progress = $client->process->getStageHex(0);

        if($client->step_id == $step->id)
            $client_progress = $client->process->getStageHex(1);

        if($client->step_id > $step->id)
            $client_progress = $client->process->getStageHex(2);

        $steps = Step::where('process_id', $client->process_id)->orderBy('order','asc')->get();
        $c_step_order = Step::where('id',$client->step_id)->withTrashed()->first();

        $step_data = [];
        foreach ($steps as $a_step):
            $progress_color = $client->process->getStageHex(0);

            if($c_step_order->order == $a_step->order)
                $progress_color = $client->process->getStageHex(1);

            if($c_step_order->order > $a_step->order)
                $progress_color = $client->process->getStageHex(2);


            $tmp_step = [
                'id' => $a_step->id,
                'name' => $a_step->name,
                'process_id' => $a_step->process_id,
                'progress_color' => $progress_color,
                'order' => $a_step->order
            ];

            array_push($step_data, $tmp_step);

        endforeach;

        if($section_id == 0) {
            $client_forms = ClientForm::where('id', $form_id)->first();
            $sections = FormSection::where('form_id', $client_forms->dynamic_form_id)->orderBy('order', 'asc')->get();
        } else {
            $sections = FormSection::where('form_id', $form_id)->orderBy('order', 'asc')->get();
        }

        $form_data = [];
        foreach ($sections as $a_step):
            $progress_color = $client->process->getStageHex(0);

            $tmp_step = [
                'id' => $a_step->id,
                'name' => $a_step->name,
                'form_id' => $a_step->form_id,
                'progress_color' => $progress_color,
                'order' => $a_step->order
            ];

            array_push($form_data, $tmp_step);

        endforeach;


        return view('clients.forms.form')->with([
            'client' => $client,
            'step'=>$step,
            'active' => $step,
            'process_progress' => $process_progress,
            'form_progress' => $form_progress,
            'form_section' => $form_section,
            'view_process_dropdown' => $client->clientProcessIfActivitiesExist(),
            'steps' => $step_data,
            'forms' => $form_data,
            'form' => $form
        ]);
    }

    public function storeDynamicForm(Client $client, Request $request){
        //dd($request);
        if($request->has('form_section_id') && $request->input('form_section_id') != ''){
            $log = new Log;
            $log->client_id = $client->id;
            $log->user_id = auth()->id();
            $log->save();

            $id = $client->id;
            $form_section = FormSection::find($request->input('form_section_id'));
            $form_section = $form_section->load(['form_section_inputs.input.data' => function ($query) use ($id) {
                $query->where('client_id', $id);
            }]);

            $all_activities_completed = false;
            foreach ($form_section->form_section_inputs as $activity) {
                if(is_null($request->input($activity->id))){
                    if($request->input('old_'.$activity->id) != $request->input($activity->id)){

                        if(is_array($request->input($activity->id))){

                            $old = explode(',',$request->input('old_'.$activity->id));
                            $diff = array_diff($old,$request->input($activity->id));
                            //dd($diff);

                            foreach($request->input($activity->id) as $key => $value) {
                                $activity_log = new FormLog;
                                $activity_log->log_id = $log->id;
                                $activity_log->input_id = $activity->id;
                                $activity_log->input_name = $activity->name;
                                $activity_log->old_value = $request->input('old_' . $activity->id);
                                $activity_log->new_value = $value;
                                $activity_log->save();
                            }
                        } else {
                            $old = $request->input('old_'.$activity->id);

                            $activity_log = new FormLog;
                            $activity_log->log_id = $log->id;
                            $activity_log->input_id = $activity->id;
                            $activity_log->input_name = $activity->name;
                            $activity_log->old_value = $request->input('old_'.$activity->id);
                            $activity_log->new_value = $request->input($activity->id);
                            $activity_log->save();
                        }

                        switch ($activity->actionable_type) {
                            case 'App\FormInputBoolean':
                                FormInputBooleanData::where('input_boolean_id',$activity->actionable_id)->where('client_id',$client->id)->delete();

                                break;
                            case 'App\FormInputDate':
                                FormInputDateData::where('input_date_id',$activity->actionable_id)->where('client_id',$client->id)->delete();

                                break;
                            case 'App\FormInputText':
                                FormInputTextData::where('input_text_id',$activity->actionable_id)->where('client_id',$client->id)->delete();

                                break;
                            case 'App\FormInputTextarea':
                                FormInputTextareaData::where('input_textarea_id',$activity->actionable_id)->where('client_id',$client->id)->delete();

                                break;
                            case 'App\FormInputDropdown':
                                FormInputDropdownData::where('input_dropdown_id',$activity->actionable_id)->where('client_id',$client->id)->delete();

                                break;
                            case 'App\FormInputRadio':
                                FormInputRadioData::where('input_radio_id',$activity->actionable_id)->where('client_id',$client->id)->delete();

                                break;
                            case 'App\FormInputCheckbox':
                                FormInputCheckboxData::where('input_dropdown_id',$activity->actionable_id)->where('client_id',$client->id)->delete();

                                break;
                            default:
                                //todo capture defaults
                                break;
                        }
                    }
                }

                if ($request->has($activity->id) && !is_null($request->input($activity->id))) {
                    //If value did not change, do not save it again or add it to log
                    if ($request->input('old_' . $activity->id) == $request->input($activity->id)) {
                        continue;
                    }
                    if(is_array($request->input($activity->id))){

                        $old = explode(',',$request->input('old_'.$activity->id));
                        $diff = array_diff($old,$request->input($activity->id));
                        //dd($diff);

                        foreach($request->input($activity->id) as $key => $value) {
                            $activity_log = new FormLog;
                            $activity_log->log_id = $log->id;
                            $activity_log->input_id = $activity->id;
                            $activity_log->input_name = $activity->name;
                            $activity_log->old_value = $request->input('old_' . $activity->id);
                            $activity_log->new_value = $value;
                            $activity_log->save();
                        }
                    } else {
                        $old = $request->input('old_'.$activity->id);

                        $activity_log = new FormLog;
                        $activity_log->log_id = $log->id;
                        $activity_log->input_id = $activity->id;
                        $activity_log->input_name = $activity->name;
                        $activity_log->old_value = $request->input('old_'.$activity->id);
                        $activity_log->new_value = $request->input($activity->id);
                        $activity_log->save();
                    }

                    //activity type hook
                    //dd($request);
                    switch ($activity->input_type) {
                        case 'App\FormInputBoolean':
                            FormInputBooleanData::where('client_id',$client->id)->where('Form_input_boolean_id',$activity->input_id)->where('data',$old)->delete();

                            FormInputBooleanData::insert([
                                'data' => $request->input($activity->id),
                                'form_input_boolean_id' => $activity->input_id,
                                'client_id' => $client->id,
                                'user_id' => auth()->id(),
                                'duration' => 120,
                                'created_at' => now()
                            ]);
                            break;
                        case 'App\FormInputDate':
                            FormInputDateData::insert([
                                'data' => $request->input($activity->id),
                                'form_input_date_id' => $activity->input_id,
                                'client_id' => $client->id,
                                'user_id' => auth()->id(),
                                'duration' => 120,
                                'created_at' => now()
                            ]);
                            break;
                        case 'App\FormInputText':

                            FormInputTextData::insert([
                                'data' => $request->input($activity->id),
                                'form_input_text_id' => $activity->input_id,
                                'client_id' => $client->id,
                                'user_id' => auth()->id(),
                                'duration' => 120,
                                'created_at' => now()
                            ]);
                            break;
                        case 'App\FormInputTextarea':

                            FormInputTextareaData::insert([
                                'data' => $request->input($activity->id),
                                'form_input_textarea_id' => $activity->input_id,
                                'client_id' => $client->id,
                                'user_id' => auth()->id(),
                                'duration' => 120,
                                'created_at' => now()
                            ]);
                            break;
                        case 'App\FormInputDropdown':
                            foreach($request->input($activity->id) as $key => $value){
                                if(in_array($value,$old,true)) {

                                } else {
                                    FormInputDropdownData::insert([
                                        'form_input_dropdown_id' => $activity->input_id,
                                        'form_input_dropdown_item_id' => $value,
                                        'client_id' => $client->id,
                                        'user_id' => auth()->id(),
                                        'duration' => 120,
                                        'created_at' => now()
                                    ]);
                                }

                                if(!empty($diff)){
                                    FormInputDropdownData::where('client_id',$client->id)->where('form_input_dropdown_id',$activity->input_id)->whereIn('form_input_dropdown_item_id',$diff)->delete();
                                }
                            }
                            break;
                        case 'App\FormInputCheckbox':
                            foreach($request->input($activity->id) as $key => $value){
                                if(in_array($value,$old,true)) {

                                } else {
                                    FormInputCheckboxData::insert([
                                        'form_input_checkbox_id' => $activity->input_id,
                                        'form_input_checkbox_item_id' => $value,
                                        'client_id' => $client->id,
                                        'user_id' => auth()->id(),
                                        'duration' => 120,
                                        'created_at' => now()
                                    ]);
                                }

                                if(!empty($diff)){
                                    FormInputCheckboxData::where('client_id',$client->id)->where('form_input_checkbox_id',$activity->input_id)->whereIn('form_input_checkbox_item_id',$diff)->delete();
                                }
                            }
                            break;
                        case 'App\FormInputRadio':
                            //foreach($request->input($activity->id) as $key => $value){
                            //dd($old);
                            FormInputRadioData::where('client_id',$client->id)->where('form_input_radio_id',$activity->input_id)->delete();
                            FormInputRadioData::insert([
                                'form_input_radio_id' => $activity->input_id,
                                'form_input_radio_item_id' => $request->input($activity->id),
                                'client_id' => $client->id,
                                'user_id' => auth()->id(),
                                'duration' => 120,
                                'created_at' => now()
                            ]);


                            /*if(!empty($diff)){
                                FormInputRadioData::where('client_id',$client->id)->where('form_input_radio_id',$activity->input_id)->whereIn('form_input_radio_item_id',$diff)->delete();
                            }*/
                            //}
                            break;
                        default:
                            //todo capture defaults
                            break;
                    }

                }
            }
        }

        $form = Forms::withTrashed()->where('id',$form_section->form_id)->first();

        $client_form = ClientForm::where('dynamic_form_id',$form->id)->where('client_id',$client->id)->get();

        if(count($client_form) == 0) {
            $document = new ClientForm();
            $document->name = $form->name;
            $document->form_type = $form->name;
            //$document->file = $form->file;
            $document->file = str_replace(' ','_',$form->name). "_" . ($client->company != null ? str_replace(' ','_',$client->company) : $client->first_name.'_'.$client->last_name) . ".pdf";
            $document->user_id = auth()->id();
            $document->dynamic_form = '1';
            $document->dynamic_form_id = $form->id;
            $document->client_id = $client->id;

            $document->save();
        }

        $filename = str_replace(' ','_',$form->name). "_" . ($client->company != null ? str_replace(' ','_',$client->company) : $client->first_name.'_'.$client->last_name) . ".pdf";

        if(isset($document->id)){
            return redirect()->back()->with(['flash_success' => 'Form details captured.<br /><a href="'.route('forms.process',['client_id'=>$client->id,'form_id'=>$document->id]).'" target="_blank">'.$filename.'</a>']);
        } else {
            $document = ClientForm::where('client_id',$client->id)->where('dynamic_form_id',$form->id)->first()->id;
            return redirect()->back()->with(['flash_success' => 'Form details captured.<br /><a href="'.route('forms.process',['client_id'=>$client->id,'form_id'=>$document]).'" target="_blank">'.$filename.'</a>']);
        }

    }

    public function editDynamicForm(Client $client,$form_id,$section_id){
//create dynamic form entry
//        $client = Client::find($client_id);

        $client_form = ClientForm::where('id',$form_id)->first();
//dd($form_id);
        $form = Forms::where('id',$client_form->dynamic_form_id)->first();

        $form_step_first = FormSection::where('form_id',$client_form->dynamic_form_id)->orderBy('order')->first()->id;

        $form_section = FormSection::find($form_step_first);

        $step = Step::withTrashed()->find($client->step_id);

        $process_progress = $client->getProcessStepProgress($step);
        $form_progress = $client->getFormsStepProgress($form_section);

        $client_progress = $client->process->getStageHex(0);

        if($client->step_id == $step->id)
            $client_progress = $client->process->getStageHex(1);

        if($client->step_id > $step->id)
            $client_progress = $client->process->getStageHex(2);

        $steps = Step::where('process_id', $client->process_id)->orderBy('order','asc')->get();
        $c_step_order = Step::where('id',$client->step_id)->withTrashed()->first();

        $step_data = [];
        foreach ($steps as $a_step):
            $progress_color = $client->process->getStageHex(0);

            if($c_step_order->order == $a_step->order)
                $progress_color = $client->process->getStageHex(1);

            if($c_step_order->order > $a_step->order)
                $progress_color = $client->process->getStageHex(2);


            $tmp_step = [
                'id' => $a_step->id,
                'name' => $a_step->name,
                'process_id' => $a_step->process_id,
                'progress_color' => $progress_color,
                'order' => $a_step->order
            ];

            array_push($step_data, $tmp_step);

        endforeach;

        $sections = FormSection::where('form_id',$client_form->dynamic_form_id)->orderBy('order','asc')->get();

        $form_data = [];
        foreach ($sections as $a_step):
            $progress_color = $client->process->getStageHex(0);

            $tmp_step = [
                'id' => $a_step->id,
                'name' => $a_step->name,
                'form_id' => $a_step->form_id,
                'progress_color' => $progress_color,
                'order' => $a_step->order
            ];

            array_push($form_data, $tmp_step);

        endforeach;


        return view('clients.forms.form')->with([
            'client' => $client,
            'step'=>$step,
            'active' => $step,
            'process_progress' => $process_progress,
            'form_progress' => $form_progress,
            'form_section' => $form_section,
            'view_process_dropdown' => $client->clientProcessIfActivitiesExist(),
            'steps' => $step_data,
            'forms' => $form_data,
            'form' => $form
        ]);
    }

    public function deleteDynamicForm(Client $client,$form){

    }

    public function getFormFirstSection($form_id){
        $form = FormSection::where('form_id',$form_id)->orderBy('order')->first()->id;

        return $form;
    }

    public function includeInputInClientBasket(Request $request)
    {
        if($request->has('checked_values')) {
            foreach ($request->input('checked_values') as $value) {
                $activity = FormSectionInputInClientBasket::where('client_id', $request->input("client_id"))->where('input_id', $value)->first() ?? new FormSectionInputInClientBasket();
                $activity->client_id = $request->input("client_id");
                $activity->input_id = $value;
                $activity->form_id = 2;
                $activity->in_client_basket = 1;
                $activity->save();
            }
        }

        if($request->has('nonchecked_values')) {
            foreach ($request->input('nonchecked_values') as $value) {
                $activity = FormSectionInputInClientBasket::where('client_id', $request->input("client_id"))->where('input_id', $value)->first() ?? new FormSectionInputInClientBasket();
                $activity->client_id = $request->input("client_id");
                $activity->input_id = $value;
                $activity->form_id = 2;
                $activity->in_client_basket = 0;
                $activity->save();
            }
        }

        if($request->has('input_id')) {
                $activity = FormSectionInputInClientBasket::where('client_id', $request->input("client_id"))->where('input_id', $request->input("input_id"))->first();
                $activity->client_id = $request->input("client_id");
                $activity->input_id = $request->input("input_id");
                $activity->form_id = 2;
                $activity->in_client_basket = 0;
                $activity->save();

            $message = 'The selected client detail was successfully removed from the basket';

            return response()->json(['message' => '1','success'=> $message]);
        }

        $message = 'All selected client details were successfully added to the basket';

        return response()->json(['message' => '1','success'=> $message]);
    }

    public function getNewForms($clientid){

        $client = Client::find($clientid);

        return response()->json($client->startNewFormDropdown());
    }
}

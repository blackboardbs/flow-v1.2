<?php

namespace App\Http\Controllers;

use App\Crm;
use App\CrmSection;
use App\CrmSectionInputInClientBasket;
use Illuminate\Http\Request;

class CrmController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
        //return $this->middleware('auth')->except('index');
    }

    public function index(Request $request){

        $crms = Crm::get();

        $parameters = [
            'crms' => $crms
        ];

        return view('crm.index')->with($parameters);
    }

    public function create(Request $request){
        return view('crm.create');
    }

    public function store(Request $request){
        $crm = new Crm();
        $crm->name = $request->input('name');
        $crm->save();

        return redirect(route('crm.show',$crm->id))->with('flash_success', 'CRM created successfully');
    }

    public function edit($crm){

        $crm = Crm::find($crm);

        $paramaters = [
            'crm' => $crm
        ];

        return view('crm.edit')->with($paramaters);
    }

    public function update($crmid,Request $request){
        $crm = Crm::find($crmid);
        if ($request->has('name')) {
            $crm->name = $request->input('name');
        }

        $crm->save();

        return redirect(route('crm.show',$crm))->with('flash_success', 'CRM updated successfully.');
    }

    public function show(Crm $crm){

        //dd($crm->load('sections'));
        $crmfields = CrmSection::with('crm_section_input.input.data')->where('crm_id',$crm->id)->get();
//dd($crmfields);
        $parameters = [
            'crms' => $crm->load('sections'),
            'crm_sections' => CrmSection::where('crm_id',$crm->id)->orderBy('order')->pluck('name','id')->prepend('Please Select','0'),
            'crmfields' => $crmfields
        ];

        return view('crm.show')->with($parameters);
    }

    public function includeInputInClientBasket(Request $request)
    {
        if ($request->has('checked_values')) {
            foreach ($request->input('checked_values') as $value) {
                $activity = CrmSectionInputInClientBasket::where('client_id', $request->input("client_id"))->where('input_id', $value)->first() ?? new CrmSectionInputInClientBasket();
                $activity->client_id = $request->input("client_id");
                $activity->input_id = $value;
                $activity->crm_id = $request->input('crm_id');
                $activity->in_client_basket = 1;
                $activity->save();
            }
        }

        if ($request->has('nonchecked_values')) {
            foreach ($request->input('nonchecked_values') as $value) {
                $activity = CrmSectionInputInClientBasket::where('client_id', $request->input("client_id"))->where('input_id', $value)->first() ?? new CrmSectionInputInClientBasket();
                $activity->client_id = $request->input("client_id");
                $activity->input_id = $value;
                $activity->crm_id = $request->input('crm_id');
                $activity->in_client_basket = 0;
                $activity->save();
            }
        }

        if ($request->has('input_id')) {
            $activity = CrmSectionInputInClientBasket::where('client_id', $request->input("client_id"))->where('input_id', $request->input("input_id"))->first();
            $activity->client_id = $request->input("client_id");
            $activity->input_id = $request->input("input_id");
            $activity->crm_id = $request->input('crm_id');
            $activity->in_client_basket = 0;
            $activity->save();

            $message = 'The selected client detail was successfully removed from the basket';

            return response()->json(['message' => '1', 'success' => $message]);
        }

        $message = 'All selected client details were successfully added to the basket';

        return response()->json(['message' => '1', 'success' => $message]);
    }
}

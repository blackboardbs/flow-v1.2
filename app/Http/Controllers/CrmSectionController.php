<?php

namespace App\Http\Controllers;

use App\Crm;
use App\CrmInputAmount;
use App\CrmInputBoolean;
use App\CrmInputDate;
use App\CrmInputDropdown;
use App\CrmInputDropdownItem;
use App\CrmInputHeading;
use App\CrmInputInteger;
use App\CrmInputSubheading;
use App\CrmInputText;
use App\CrmInputTextarea;
use App\CrmInputVideo;
use App\CrmSection;
use App\CrmSectionInputs;
use App\FormInputAmount;
use App\FormInputPercentage;
use App\FormInputSubheading;
use App\Role;
use Illuminate\Http\Request;

class CrmSectionController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
        //return $this->middleware('auth')->except('index');
    }

    public function create(Crm $crm){
        return view('crm.sections.create')->with(['crms' => $crm->load('sections')]);
    }

    public function store(Request $request,Crm $crm){
        //dd($request);
        $section = new CrmSection();
        $section->name = $request->input('name');
        $section->show_name_in_tabs = $request->input('show_name_in_tab');
        $section->group = $request->input('group_section');
        $section->crm_id = $crm->id;
        $section->order = CrmSection::where('crm_id', $crm->id)->max('order') + 1;
        $section->save();

        //loop over each activity input
        foreach ($request->input('inputs') as $input_key => $input_input) {
            $input = new CrmSectionInputs();
            $crm_input = $this->createInput($input_input['type']);

            $input->name = $input_input['name'];
            $input->order = $input_key + 1;
            $input->input_id = $crm_input->id;
            $input->input_type = $this->getInputType($input_input['type']);
            $input->crm_section_id = $section->id;
            $input->kpi = (isset($input_input['kpi']) && $input_input['kpi'] == "on") ? 1 : null;
            if ($input_input['type'] == 'heading' || $input_input['type'] == 'subheading') {
                $input->client_bucket = 1;
            } else {
                $input->client_bucket = (isset($input_input['client_bucket']) && $input_input['client_bucket'] == "on") ? 1 : 0;
            }
            $input->level = (isset($input_input['level']) ? $input_input['level'] : 0);
            $input->color = (isset($input_input['color']) && $input_input['color'] != '#hsla(0,0%,0%,0)' ? $input_input['color'] : null);
            $input->save();

            //if activity is a dropdown type
            if ($input_input['type'] == 'dropdown') {

                //only add dropdown items if there is input
                if (isset($input_input['dropdown_items'])) {
                    //dd($input_input['dropdown_items']);
                    //loop over each dropdown item
                    foreach ($input_input['dropdown_items'] as $dropdown_item) {
                        $actionable_dropdown_item = new CrmInputDropdownItem;
                        $actionable_dropdown_item->crm_input_dropdown_id = $crm_input->id;
                        $actionable_dropdown_item->name = $dropdown_item;
                        $actionable_dropdown_item->save();
                    }
                }
            }

        }

        return redirect(route('crm.show', $crm->id))->with('flash_success', 'CRM Section successfully saved.');
    }

    public function edit($crmid){

        $crm = CrmSection::find($crmid);


        $section_inputs_array = [];
        foreach ($crm->crm_section_inputs as $inputs) {

            $section_input_array = [
                'id' => $inputs->id,
                'name' => $inputs->name,
                'tooltip' => $inputs->tooltip,
                'type' => $inputs->getCrmTypeName(),
                'kpi' => ($inputs->kpi ==1 ? true : false),
                'level' => $inputs->level,
                'color' => $inputs->color,
                'client_bucket' => ($inputs->client_bucket == "1" ? true : false),
                'dropdown_item' => '',
                'dropdown_items' => [],
                'is_grouping_items_shown' => false,
                'grouping_value' => ($inputs->grouping != null ? $inputs->grouping : 0),
                'is_dropdown_items_shown' => false,
                'radio_item' => '',
                'radio_items' => [],
                'is_tooltip_shown' => false,
                'is_radio_items_shown' => false,
                'checkbox_item' => '',
                'checkbox_items' => [],
                'is_checkbox_items_shown' => false
            ];

            if ($inputs->getCrmTypeName() == 'dropdown') {

                $section_input_array['dropdown_items'] = CrmInputDropdownItem::where('crm_input_dropdown_id',$inputs->input_id)->pluck('name')->toArray();
            }

            array_push($section_inputs_array, $section_input_array);
        }

        $paramaters = [
            'crm' => $crm,
            'inputs' => json_encode($section_inputs_array),
            'roles' => Role::orderBy('name')->get()
        ];


        return view('crm.sections.edit')->with($paramaters);
    }

    public function update(CrmSection $crm_section,Request $request){
        /*dd($request);
        $form_section = FormSection::find($form_sections->id);*/
        $existing_section = CrmSection::where('name',$crm_section->name)->first();

        if($existing_section != null){
            $section_id = $existing_section->id;

            $crm_section->name = $request->input('name');
            $crm_section->show_name_in_tabs = $request->input('show_name_in_tab');
            $crm_section->group = $request->input('group_section');
            $crm_section->save();
        }
        //dd($request->input('activities'));
        $pinputs = array();
        if($request->input("inputs") != null) {
            foreach ($request->input("inputs") as $input) {
                //dd($activities);
                array_push($pinputs, $input["id"]);
            }
        }
        CrmSectionInputs::where('crm_section_id',$crm_section->id)->whereNotIn('id',$pinputs)->delete();


        //loop over each activity input
        if($request->input("inputs") != null) {
            foreach ($request->input('inputs') as $activity_key => $activity_input) {

                $activity = $crm_section->crm_section_inputs()->where('id', $activity_input['id'])->get()->first();
                $activity_type = $crm_section->crm_section_inputs()->where('id', $activity_input['id'])->where('input_type', $this->getInputType($activity_input['type']))->get()->first();

                //if there is a previous activity matching the name and type, reactivate it else create a new one
                if (!$activity) {
                    $new_activity = true;
                    if (!$activity_type) {
                        $new_activity_type = true;
                        $activity = new CrmSectionInputs;
                        $actionable = $this->createInput($activity_input['type']);
                    } else {
                        $new_activity_type = false;
                        $activity->restore();
                        $actionable = $activity->input;
                    }

                } else {
                    $new_activity = false;
                    if (!$activity_type) {
                        $new_activity_type = true;
                        $activity = CrmSectionInputs::find($activity_input['id']);
                        $actionable = $this->createInput($activity_input['type']);
                    } else {
                        $new_activity_type = false;
                        $activity->restore();
                        $actionable = $activity->input;
                    }

                }

                $activity->name = $activity_input['name'];
                $activity->order = $activity_key + 1;
                $activity->input_id = (isset($actionable->id) ? $actionable->id : $actionable);
                $activity->input_type = $this->getInputType($activity_input['type']);
                $activity->crm_section_id = $crm_section->id;
                $activity->grouped = (isset($activity_input['grouping']) && $activity_input['grouping'] == "on") ? 1 : 0;
                $activity->grouping = (isset($activity_input['grouping_value'])) ? $activity_input['grouping_value'] : 0;
                $activity->kpi = (isset($activity_input['kpi']) && $activity_input['kpi'] == "on") ? 1 : null;
                if ($activity_input['type'] == 'heading' || $activity_input['type'] == 'subheading') {
                    $activity->client_bucket = 1;
                } else {
                    $activity->client_bucket = (isset($activity_input['client_bucket']) && $activity_input['client_bucket'] == "on") ? 1 : 0;
                }
                $activity->level = (isset($activity_input['level']) ? $activity_input['level'] : 0);
                $activity->color = (isset($activity_input['color']) && $activity_input['color'] != '#hsla(0,0%,0%,0)' ? $activity_input['color'] : null);
                $activity->save();


                //if activity is a dropdown type
                if ($activity_input['type'] == 'dropdown') {

                    //delete all previous dropdown items
                    CrmInputDropdownItem::where('crm_input_dropdown_id', (isset($actionable->id) ? $actionable->id : $actionable))->delete();


                    //only add dropdown items if there is input
                    if (isset($activity_input['dropdown_items'])) {

                        //loop over each dropdown item
                        foreach ($activity_input['dropdown_items'] as $dropdown_item) {

                            //if this is a reactivated activity, search for all old dropdowns
                            if (!$new_activity_type) {

                                //find if there already a dropdown item for that activity
                                $item = $actionable->items()->withTrashed()->where('name', $dropdown_item)->get()->first();

                                //if there is a previous dropdown item, reactivate it else create a new one
                                if (!$item) {
                                    $actionable_dropdown_item = new CrmInputDropdownItem;
                                    $actionable_dropdown_item->form_input_dropdown_id = $actionable->id;
                                    $actionable_dropdown_item->name = $dropdown_item;
                                    $actionable_dropdown_item->save();
                                } else {
                                    $item->restore();
                                }

                            } // otherwise create a new dropdown item without searching
                            else {
                                $actionable_dropdown_item = new CrmInputDropdownItem;
                                $actionable_dropdown_item->form_input_dropdown_id = (isset($actionable->id) ? $actionable->id : $actionable);
                                $actionable_dropdown_item->name = $dropdown_item;
                                $actionable_dropdown_item->save();
                            }
                        }
                    }
                }

            }
        }
        return redirect(route('crm.show', $crm_section->crm_id))->with('flash_success', 'Crm updated successfully.');
    }

    public function getInputType($type)
    {
        //activity type hook
        switch ($type) {
            case 'text':
                return 'App\CrmInputText';
                break;
            case 'heading':
                return 'App\CrmInputHeading';
                break;
            case 'subheading':
                return 'App\CrmInputSubheading';
                break;
            case 'amount':
                return 'App\CrmInputAmount';
                break;
            case 'percentage':
                return 'App\CrmInputPercentage';
                break;
            case 'integer':
                return 'App\CrmInputInteger';
                break;
            case 'video':
                return 'App\CrmInputVideo';
                break;
            case 'textarea':
                return 'App\CrmInputTextarea';
                break;
            case 'dropdown':
                return 'App\CrmInputDropdown';
                break;
            case 'radio':
                return 'App\CrmInputRadio';
                break;
            case 'checkbox':
                return 'App\CrmInputCheckbox';
                break;
            case 'date':
                return 'App\CrmInputDate';
                break;
            case 'boolean':
                return 'App\CrmInputBoolean';
                break;
            default:
                abort(500, 'Error');
                break;
        }
    }

    public function createInput($type)
    {
        //activity type hook
        switch ($type) {
            case 'text':
                return CrmInputText::create();
                break;
            case 'heading':
                return CrmInputHeading::create();
                break;
            case 'subheading':
                return CrmInputSubheading::create();
                break;
            case 'amount':
                return CrmInputAmount::create();
                break;
            case 'percentage':
                return CrmInputPercentage::create();
                break;
            case 'integer':
                return CrmInputInteger::create();
                break;
            case 'video':
                return CrmInputVideo::create();
                break;
            case 'textarea':
                return CrmInputTextarea::create();
                break;
            case 'dropdown':
                return CrmInputDropdown::create();
                break;
            case 'date':
                return CrmInputDate::create();
                break;
            case 'boolean':
                return CrmInputBoolean::create();
                break;
            default:
                abort(500, 'Error');
                break;
        }
    }

    public function getSections(Request $request){

        $sections = CrmSection::where('crm_id',$request->input('crm_id'))->orderBy('order')->get();

        foreach ($sections as $p){
            $section[$p->id] = $p->name;
        }
        return $section;
    }
}

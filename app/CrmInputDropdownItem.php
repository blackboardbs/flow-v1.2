<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CrmInputDropdownItem extends Model
{
    use SoftDeletes;

    public function crm_section_input()
    {
        return $this->morphOne('App\CrmSectionInputs', 'input');
    }

    public function items()
    {
        return $this->hasMany('App\CrmInputDropdownItem');
    }
}

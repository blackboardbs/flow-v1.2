@extends('flow.default')

@section('title') Roles @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('roles.create')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-plus"></i> Role</a>
    </div>
@endsection

@section('content')
    <div class="content-container page-content">
        <div class="row col-md-12 h-100 pr-0">
            @yield('header')
            <div class="container-fluid index-container-content">
                <div class="table-responsive h-100">
                    <table class="table table-bordered table-sm table-hover">
                        <thead>
                        <tr>
                            <th>Name</th>
                            @foreach($permissions as $permission)
                                <th>{{$permission->display_name}}</th>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($roles as $role)
                            <tr>
                                <td>{{$role->display_name}}</td>
                                @foreach($permissions as $permission)
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input name="permission[{{$role->id.']['.$permission->id}}]" {{($role->permissions->where('id',$permission->id)->count() > 0) ? 'checked' : ''}} type="checkbox" class="custom-control-input" id="permission[{{$role->id.']['.$permission->id}}]" value="{{$permission->id}}">
                                            <label class="custom-control-label" for="permission[{{$role->id.']['.$permission->id}}]">&nbsp;</label>
                                        </div>
                                    </td>
                                @endforeach
                            </tr>
                        @empty
                            <tr>
                                <td colspan="100%" class="text-center">No roles match those criteria.</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>

                <div class="blackboard-fab mr-3 mb-3">
                    <button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-save"></i> Save</button>
                </div>

                {{Form::close()}}
            </div>
        </div>
</div>
@endsection

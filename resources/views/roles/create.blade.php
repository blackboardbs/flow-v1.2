@extends('flow.default')

@section('title') Create Role @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('roles.index')}}" class="btn btn-outline-light float-right"><i class="fa fa-caret-left"></i> Back</a>
    </div>
@endsection

@section('content')
    <div class="content-container">
        <div class="row col-md-12">
            @yield('header')
            <div class="container-fluid">
                {{Form::open(['url' => route('roles.store'), 'method' => 'post'])}}

                <div class="form-group mt-3">
                    {{Form::label('name', 'Name')}}
                    {{Form::text('name',old('name'),['class'=>'form-control'. ($errors->has('name') ? ' is-invalid' : ''),'placeholder'=>'Name'])}}
                    @foreach($errors->get('name') as $error)
                        <div class="invalid-feedback">
                            {{ $error }}
                        </div>
                    @endforeach
                </div>

                <div class="table-responsive">
                    <table class="table table-bordered table-sm table-hover text-center">
                        <thead class="btn-dark">
                        <tr>
                            @foreach($permissions as $permission)
                                <th>{{$permission->display_name}}</th>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            @foreach($permissions as $permission)
                                <td>
                                    <div class="custom-control custom-checkbox">
                                        <input name="permission[{{$permission->id}}]" type="checkbox" class="custom-control-input" id="permission[{{$permission->id}}]" value="{{$permission->id}}">
                                        <label class="custom-control-label" for="permission[{{$permission->id}}]">&nbsp;</label>
                                    </div>
                                </td>
                            @endforeach
                        </tr>
                        </tbody>
                    </table>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-sm">Save</button>
                </div>

                {{Form::close()}}
            </div>
        </div>
    </div>
@endsection
@extends('flow.default')

@section('title') Forms @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="nav-btn-group">
            <form autocomplete="off">
                <div class="form-row">
                    <div class="form-group mt-2">
                        <div class="input-group">
                            {{Form::search('q',old('query'),['class'=>'form-control search','placeholder'=>'Search...'])}}
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="ml-2 mt-3">
                        <div class="btn-group">
                            <a href="{{route('forms.create',(isset($form_group->id) ? $form_group->id : '0'))}}" class="btn btn-primary btn-sm">Form</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('content')
    <div class="content-container page-content">
        <div class="row col-md-12 h-100 pr-0">
            @yield('header')
            <div class="container-fluid index-container-content">
                <div class="table-responsive h-100">
                    <table class="table table-bordered table-sm table-hover">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Sections</th>
                            <th>Created</th>
                            <th>Modified</th>
                            <th class="last">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($forms as $form)
                            <tr>
                                <td>{{$form->name}}</td>
                                <td>{{count($form->sections)}}</td>
                                <td>{{$form->created_at->diffForHumans()}}</td>
                                <td>{{$form->updated_at->diffForHumans()}}</td>
                                <td class="last">
                                    <a href="javascript:void(0)" onclick="copyForm({{$form->id}})" class="btn btn-default btn-sm"><i class="fas fa-copy"></i></a>
                                    {{--<a href="{{route("process.copy", $process)}}" class="btn btn-default btn-sm"><i class="fas fa-copy"></i></a>--}}
                                    <a href="{{route('forms.show',[$form_group,$form])}}" class="btn btn-info btn-sm"><i class="fas fa-eye"></i> </a>
                                    <a href="{{route('forms.edit',['formgroup' => $form_group,'form' => $form])}}" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i></a>
                                    {{Form::open(['url' => route('forms.destroy',['formgroup'=>(isset($process->fgroup) ? $process->fgroup->id : 0),'form' => $form,'formid' => $form]), 'method' => 'delete','style'=>'display:inline;width:fit-content;margin:0px;'])}}
                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                    {{Form::close()}}
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="100%" class="text-center"><small class="text-muted">No Processes match those criteria.</small></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
            </div>
        </div>
    </div>
@endsection

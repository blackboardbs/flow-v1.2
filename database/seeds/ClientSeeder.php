<?php

use App\ActionableBooleanData;
use App\ActionableDateData;
use App\ActionableDocumentData;
use App\ActionableDropdownData;
use App\ActionableDropdownItem;
use App\ActionableNotificationData;
use App\ActionableTemplateEmailData;
use App\ActionableTextData;
use App\Client;
use App\ClientComment;
use App\FormInputDate;
use App\FormInputDateData;
use App\FormInputDropdownData;
use App\FormInputTextData;
use App\Step;
use Illuminate\Database\Seeder;
use League\Csv\Reader;
use League\Csv\Statement;
use Carbon\Carbon;

class ClientSeeder extends Seeder
{
    public function __construct() {
        //
    }

    public function run()
    {
        /*$this->getClientFiles('Gaukes_Moster_Data_New.csv', 45);
        $this->getClientFiles('Philip_Roesch_Data_New.csv', 65);
        $this->getClientFiles('Rayno_van_Vuuren_Data_New.csv', 47);
        $this->getClientFiles('Stian_de_Witt_Data_New.csv', 43);*/
        $this->getClientFiles('attoh_corp.csv', 2);
    }

    public function getClientFiles($fileName, $office_id)
    {
        $csv = Reader::createFromPath(database_path('/data/'.$fileName, 'r'));
        $csv->setDelimiter(';');
        $csv->setHeaderOffset(0);
        $stmt = (new Statement());
        $records = $stmt->process($csv);

        foreach ($records as $record_key => $record) {
            $client = new Client;
            $client->first_name = isset($record['Trade Name']) && $record['Trade Name'] != '' && $record['Trade Name'] != 'NULL' ? utf8_decode($record['Trade Name']) : NULL;
            $client->last_name = isset($record['Client_Surname']) && $record['Client_Surname'] != '' && $record['Client_Surname'] != 'NULL' ? $record['Client_Surname'] : NULL;
            $client->initials = isset($record['Client_Initials']) && $record['Client_Initials'] != '' && $record['Client_Initials'] != 'NULL' ? $record['Client_Initials'] : NULL;
            $client->email = isset($record['Client_Email']) && $record['Client_Email'] != '' && $record['Client_Email'] != 'NULL' ? $record['Client_Email'] : NULL;
            $client->contact = isset($record['Client_Cell']) && $record['Client_Cell'] != '' && $record['Client_Cell'] != 'NULL' ? $record['Client_Cell'] : NULL;
            $client->id_number = isset($record['IDNO']) && $record['IDNO'] != '' && $record['IDNO'] != 'NULL' ? $record['IDNO'] : NULL;
            $client->introducer_id = 1;
            $client->process_id = 35;
            $client->step_id = 188;
            $client->office_id = $office_id;
            $client->created_at = now();
            $client->save();

            $cp = new \App\ClientProcess;
            $cp->client_id = $client->id;
            $cp->process_id = 35;
            $cp->step_id = 188;
            $cp->active = 0;
            $cp->save();

            // Cellphone Number = 378
            $clientCellPhoneNumber = isset($record['Cat']) && $record['Cat'] != '' && $record['Cat'] != 'NULL' ? $record['Cat'] : NULL;
            $formInputDateData = new FormInputTextData();
            $formInputDateData->form_input_text_id = 415;
            $formInputDateData->data = trim($clientCellPhoneNumber);
            $formInputDateData->client_id = $client->id;
            $formInputDateData->user_id = 1;
            $formInputDateData->duration = 120;
            $formInputDateData->save();

            // TelNumber - Work = 20
            $clientTelWorkNumber = isset($record['Trade Name']) && $record['Trade Name'] != '' && $record['Trade Name'] != 'NULL' ? $record['Trade Name'] : NULL;
            $formInputDateData = new FormInputTextData();
            $formInputDateData->form_input_text_id = 410;
            $formInputDateData->data = trim($clientTelWorkNumber);
            $formInputDateData->client_id = $client->id;
            $formInputDateData->user_id = 1;
            $formInputDateData->duration = 120;
            $formInputDateData->save();

            /*// ClientFaxHome = 377
            $ClientFaxHome = isset($record['Client_FaxHome']) && $record['Client_FaxHome'] != '' && $record['Client_FaxHome'] != 'NULL' ? $record['Client_FaxHome'] : NULL;
            $formInputDateData = new FormInputTextData();
            $formInputDateData->form_input_text_id = 377;
            $formInputDateData->data = trim($ClientFaxHome);
            $formInputDateData->client_id = $client->id;
            $formInputDateData->user_id = 1;
            $formInputDateData->duration = 120;
            $formInputDateData->save();*/

            // TelNumber - Home = 19
            $clientTelHomeNumber = isset($record['City']) && $record['City'] != '' && $record['City'] != 'NULL' ? $record['City'] : NULL;
            $formInputDateData = new FormInputTextData();
            $formInputDateData->form_input_text_id = 416;
            $formInputDateData->data = trim($clientTelHomeNumber);
            $formInputDateData->client_id = $client->id;
            $formInputDateData->user_id = 1;
            $formInputDateData->duration = 120;
            $formInputDateData->save();

            // Date of Birth = 65
            $clientDateOfBirth = isset($record['Province']) && $record['Province'] != '' && $record['Province'] != 'NULL' ? $record['Province'] : NULL;
            $formInputDateData = new FormInputTextData();
            $formInputDateData->form_input_text_id = 417;
            $formInputDateData->data = date('y-m-d', strtotime($clientDateOfBirth));
            $formInputDateData->client_id = $client->id;
            $formInputDateData->user_id = 1;
            $formInputDateData->duration = 120;
            $formInputDateData->save();

            // Client Address 1 - 23
            $clientAddressLine1 = isset($record['Fund Consultant (Client Advisor)']) && $record['Fund Consultant (Client Advisor)'] != '' && $record['Fund Consultant (Client Advisor)'] != 'NULL' ? $record['Fund Consultant (Client Advisor)'] : NULL;
            $formInputDateData = new FormInputTextData();
            $formInputDateData->form_input_text_id = 418;
            $formInputDateData->data = $clientAddressLine1;
            $formInputDateData->client_id = $client->id;
            $formInputDateData->user_id = 1;
            $formInputDateData->duration = 120;
            $formInputDateData->save();

            $clientAddressLine2 = isset($record['Office Admin on EB']) && $record['Office Admin on EB'] != '' && $record['Office Admin on EB'] != 'NULL' ? $record['Office Admin on EB'] : NULL;
            $formInputDateData = new FormInputTextData();
            $formInputDateData->form_input_text_id = 419;
            $formInputDateData->data = $clientAddressLine2;
            $formInputDateData->client_id = $client->id;
            $formInputDateData->user_id = 1;
            $formInputDateData->duration = 120;
            $formInputDateData->save();

            $clientAddressLine3 = isset($record['Office Admin on HC']) && $record['Office Admin on HC'] != '' && $record['Office Admin on HC'] != 'NULL' ? $record['Office Admin on HC'] : NULL;
            $formInputDateData = new FormInputTextData();
            $formInputDateData->form_input_text_id = 420;
            $formInputDateData->data = $clientAddressLine3;
            $formInputDateData->client_id = $client->id;
            $formInputDateData->user_id = 1;
            $formInputDateData->duration = 120;
            $formInputDateData->save();

            // Title : 9 - Mr, 10 - Mrs, 11 - Prof, 12 - Dr, 13 - Miss, 121 - Ms
            $clientTitle = isset($record['Parent or Branch']) && $record['Parent or Branch'] != '' && $record['Parent or Branch'] != 'NULL' ? $record['Parent or Branch'] : NULL;
            $title_id = null;
            switch (trim($clientTitle)){
                case 'Parent';
                    $title_id = 138;
                    break;
                case 'Branch';
                    $title_id = 139;
                    break;
            }

            if(isset($title_id)) {
                $formInputDateData = new FormInputDropdownData();
                $formInputDateData->form_input_dropdown_id = 36;
                $formInputDateData->form_input_dropdown_item_id = $title_id;
                $formInputDateData->client_id = $client->id;
                $formInputDateData->user_id = 1;
                $formInputDateData->duration = 120;
                $formInputDateData->save();
            }
        }
    }
}
